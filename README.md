Como iniciar o projeto?

Primeiramente, realize as seguintes ações na console:

> bundle install

> rake db:migrate

> rails server

Após o servidor ser ligado, é recomendado visualizar a aprentação em video
feita, encontrada no seguinte link:

https://youtu.be/vmxHvjLehLs


