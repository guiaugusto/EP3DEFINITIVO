class Loja < ApplicationRecord
	
	has_many :user_loja
	has_many :reviews


  has_attached_file :img_loja, styles: { lojas: "250x350>", lojas_show: "325x475>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :img_loja, content_type: /\Aimage\/.*\z/

end