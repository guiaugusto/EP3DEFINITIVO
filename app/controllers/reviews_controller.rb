class ReviewsController < ApplicationController

	before_action :achar_loja
	before_action :find_review, only: [:edit, :update, :destroy]
	before_action :authenticate_user!, only: [:new, :edit]
	#before_action :authenticate_user_loja!, only: [:new, :edit, :destroy]
	

  	def new
		@review = Review.new
	end


	def create

		if user_signed_in?
		
		@review = Review.new(review_params)
		@review.loja_id = @loja.id
		@review.user_id = current_user.id

		end


		if user_loja_signed_in?

		@review = Review.new(review_params)
		@review.loja_id = @loja.id
		@review.user_loja_id = current_user_loja.id

		end

		if @review.save
			redirect_to loja_path(@loja)
		else	
			render 'new'	
		end

	end

	def edit
	end

	def update

		if @review.update(review_params)
			redirect_to loja_path(@loja)
		else
			render 'edit'
		end
	end

	def destroy
		@review.destroy
		redirect_to loja_path(@loja)
	end


	private

		def review_params
			params.require(:review).permit(:rating, :comment)
		end

		def achar_loja
			@loja = Loja.find(params[:loja_id])
		end

		def find_review
			@review = Review.find(params[:id])
		end
end
