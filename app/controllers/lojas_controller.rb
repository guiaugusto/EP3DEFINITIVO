class LojasController < ApplicationController

before_action :achar_loja, only: [:show, :edit , :update, :destroy]
before_action :authenticate_user_loja!, only: [:new, :edit, :destroy] or before_action :authenticate_user!, only: [:index, :show]

def index
	@lojas = Loja.all.order("nome ASC")
end

def show 
	if @loja.reviews.blank?
		@average_review = 0
	else
		@average_review = @loja.reviews.average(:rating).round(2)
	end
end

def new
	@loja = Loja.new
end

def create
	@loja = Loja.new(loja_params)	

	if @loja.save
		redirect_to loja_path(@loja)	
	else
		render 'new'
	end
end

def edit
end

def update
	if @loja.update(loja_params)
		redirect_to loja_path(@loja)
	else
		render 'edit'
	end	

end

def destroy
	@loja.destroy
	redirect_to lojas_path
end

private

	def loja_params
		params.require(:loja).permit(:nome, :descricao,:email, :img_loja)
	end

	def achar_loja
		@loja = Loja.find(params[:id])
	end


end
