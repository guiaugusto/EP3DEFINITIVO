class HomesController < ApplicationController
  before_action :set_home, only: [:show, :edit, :update, :destroy]


  def index
    @homes = Home.all
  end

  def show
  end

 
  def new
    @home = Home.new
  end

  def edit
  end

  private

    def set_home
      @home = Home.find(params[:id])
    end

    def home_params
      params.fetch(:home, {})
    end
end
