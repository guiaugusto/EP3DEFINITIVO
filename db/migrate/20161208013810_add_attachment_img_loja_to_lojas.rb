class AddAttachmentImgLojaToLojas < ActiveRecord::Migration
  def self.up
    change_table :lojas do |t|
      t.attachment :img_loja
    end
  end

  def self.down
    remove_attachment :lojas, :img_loja
  end
end
