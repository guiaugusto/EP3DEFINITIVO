class AddLojaIdToReviews < ActiveRecord::Migration[5.0]
  def change
    add_column :reviews, :loja_id, :integer
  end
end
